----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/13/2016 03:39:54 PM
-- Design Name: 
-- Module Name: Adder8Inputs_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder8Inputs_tb is
--  Port ( );
end Adder8Inputs_tb;

architecture Behavioral of Adder8Inputs_tb is

    component Adder8Inputs is
        generic(INPUT_LENGTH: INTEGER);
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            B           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            C           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            D           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            E           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            F           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            G           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            H           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            
            result      : OUT STD_LOGIC_VECTOR(2048  downto 0)
        );
    end component;
    
    signal A, B, C, D, E, F, G, H   : STD_LOGIC_VECTOR(1824 downto 0);
    signal result                   : STD_LOGIC_VECTOR(2048 downto 0);

    signal clk, rst    : STD_LOGIC;
    constant CLK_PERIOD  : time := 20 ns;
    constant SIM_TIME    : time := 10000 ns;

begin

    uut: Adder8Inputs
    generic map(
        INPUT_LENGTH => 1825
    )
    port map(
        clk         => clk,
        rst         => rst,
        
        A     => A,
        B     => B,
        C     => C,
        D     => D,
        E     => E,
        F     => F,
        G     => G,
        H     => H,
        
        result      => result
    );

    clk_process: process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;  
        clk <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    simulation: process
    begin   
        rst <= '1';
        wait for CLK_PERIOD;
        rst <= '0';
        
        A <= (1824 downto 10 => '0') & "0000000001";
        B <= (1824 downto 10 => '0') & "0000000010";
        C <= (1824 downto 10 => '0') & "0000000100";
        D <= (1824 downto 10 => '0') & "0000001000";
        E <= (1824 downto 10 => '0') & "0000010000";
        F <= (1824 downto 10 => '0') & "0000100000";
        G <= (1824 downto 10 => '0') & "1001000000";
        H <= (1824 downto 10 => '0') & "1010000000";
        
        wait for 25*CLK_PERIOD;
        
        A <= (others => '1');
        B <= (others => '1');
        C <= (others => '1');
        D <= (others => '1');
        E <= (others => '1');
        F <= (others => '1');
        G <= (others => '1');
        H <= (others => '1');

        wait for SIM_TIME;
    end process;
    
    verification: process
    begin
        wait for 20 * CLK_PERIOD;
        assert (result /= ((2048 downto 11 => '0') & "10011111111"))  report "***** Test 1 PASSED  *****" severity note;
        assert (result = ((2048 downto 11 => '0') & "10011111111")) report "***** Test 1 FAILURE *****" severity failure;
        
        wait for 35 * CLK_PERIOD;
        assert (result /= ((2048 downto 1828 => '0') & (1827 downto 3 => '1') & "000"))  report "***** Test 2 PASSED  *****" severity note;
        assert (result =  ((2048 downto 1828 => '0') & (1827 downto 3 => '1') & "000")) report "***** Test 2 FAILURE *****" severity failure;
        
        wait for SIM_TIME;
    end process;
    
end Behavioral;
