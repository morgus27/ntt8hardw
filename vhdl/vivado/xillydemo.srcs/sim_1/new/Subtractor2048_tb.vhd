----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/14/2016 09:15:15 PM
-- Design Name: 
-- Module Name: Adder2048_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Subtractor2048_tb is
--  Port ( );
end Subtractor2048_tb;

architecture Behavioral of Subtractor2048_tb is

    component Subtractor2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;
    
    signal A, B         : STD_LOGIC_VECTOR(2047 downto 0);
    signal S            : STD_LOGIC_VECTOR(2048 downto 0);

    signal clk, rst    : STD_LOGIC;
    constant CLK_PERIOD  : time := 20 ns;
    constant SIM_TIME    : time := 10000 ns;
    
begin

    uut: Subtractor2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => A,
        B           => B,
        S           => S
    );

    clk_process: process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;  
        clk <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    simulation: process
    begin   
        rst <= '1';
        wait for CLK_PERIOD;
        rst <= '0';
        
        A <= (others => '1');
        B <= (others => '1');
        
        wait for 20 * CLK_PERIOD;
        
        A <= (2047 downto 1825 => '0') & "1" & (1823 downto 1 => '0') & '1';
        B <= (2047 downto 1825 => '0') & "1" & (1823 downto 2 => '0') & "11";
        
       wait for SIM_TIME;
    end process;

    verification: process
    begin
        wait for 20 * CLK_PERIOD;
        assert (S /= (2048 downto 0 => '0'))  report "***** Test 1 PASSED  *****" severity note;
        assert (S =  (2048 downto 0 => '0'))  report "***** Test 1 FAILURE *****" severity failure;
        
        wait for 30 * CLK_PERIOD;
        
        assert (S /= (2048 downto 1 => '1') & '0')  report "***** Test 2 PASSED  *****" severity note;
        assert (S =  (2048 downto 1 => '1') & '0')  report "***** Test 2 FAILURE *****" severity failure;
        
        wait for SIM_TIME;
    end process;

end Behavioral;
