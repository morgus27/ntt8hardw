----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/13/2016 03:39:54 PM
-- Design Name: 
-- Module Name: Adder8Inputs_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Reducer_tb is
--  Port ( );
end Reducer_tb;

architecture Behavioral of Reducer_tb is

    component Reducer is
    port (
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        input       : IN STD_LOGIC_VECTOR(3648 downto 0);
        output      : OUT STD_LOGIC_VECTOR(1824 downto 0)
    );
    end component;

    signal input                    : STD_LOGIC_VECTOR(3648 downto 0);
    signal output                   : STD_LOGIC_VECTOR(1824 downto 0);

    signal clk, rst    : STD_LOGIC;
    constant CLK_PERIOD  : time := 20 ns;
    constant SIM_TIME    : time := 10000 ns;

begin

    uut: Reducer
    port map(
        clk         => clk,
        rst         => rst,
        
        input       => input,
        output      => output
    );

    clk_process: process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;  
        clk <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    simulation: process
    begin   
        rst <= '1';
        wait for CLK_PERIOD;
        rst <= '0';
        
        input <= (3648 downto 1825 => '0') & '1' & (1823 downto 2 => '0') & "11";
        
        wait for 50*CLK_PERIOD;
        input <= '1' & (3647 downto 1825 => '0') & '1' & (1823 downto 2 => '0') & "11";
                
        wait for 50*CLK_PERIOD;
        input <= (others => '1');
        
        wait for 50*CLK_PERIOD;
        input <= '0' & (3647 downto 0 => '1');
        
        wait for 50*CLK_PERIOD;
        input <= '0' & (3647 downto 1 => '1') & '0';
        
        wait for 50*CLK_PERIOD;
        input <= (3648 downto 1 => '1') & '0';
        
        wait for 50*CLK_PERIOD;
        input <= (3648 downto 1000 => '0') & (999 downto 0 => '1');

        wait for SIM_TIME;
    end process;
    
    verification: process
    begin
        wait for 50 * CLK_PERIOD;
        -- inputReg[3648] == 0, subResultReg[1824]== 0, comparatorSig == 0 => should be: result = subResultReg
        assert (output /= ((1824 downto 2 => '0') & "10"))  report "***** Test 1 PASSED  *****" severity note;
        assert (output =  ((1824 downto 2 => '0') & "10"))  report "***** Test 1 FAILURE *****" severity failure;
        
        wait for 50*CLK_PERIOD;
        -- inputReg[3648] == 1, subResultReg[1824]== 0, comparatorSig == 0 => should be: result = subResultReg-1
        assert (output /= (1824 downto 1 => '0') & '1')  report "***** Test 2 PASSED  *****" severity note;
        assert (output =  (1824 downto 1 => '0') & '1')  report "***** Test 2 FAILURE *****" severity failure;
        
        wait for 50*CLK_PERIOD;
        -- inputReg[3648] == 1, subResultReg[1824]== 0, comparatorSig == 1 => should be: result = "1000...000"
        assert (output /= ('1' & (1823 downto 0 => '0')))  report "***** Test 3 PASSED  *****" severity note;
        assert (output =  ('1' & (1823 downto 0 => '0')))  report "***** Test 3 FAILURE *****" severity failure;
        
        wait for 50*CLK_PERIOD;
        -- inputReg[3648] == 0, subResultReg[1824]== 0, comparatorSig == 1 => should be: result = 0
        assert (output /= (1824 downto 0 => '0'))  report "***** Test 4 PASSED  *****" severity note;
        assert (output = (1824 downto 0 => '0'))  report "***** Test 4 FAILURE *****" severity failure;
        
        wait for 50*CLK_PERIOD;
        -- inputReg[3648] == 0, subResultReg[1824]== 1, comparatorSig == 0 => should be: result = subResultReg+"100.001"
        assert (output /= ('1' & (1823 downto 0 => '0')))  report "***** Test 5 PASSED  *****" severity note;
        assert (output =  ('1' & (1823 downto 0 => '0')))  report "***** Test 5 FAILURE *****" severity failure;
        
        wait for 50*CLK_PERIOD;                                                                                                                   
        -- inputReg[3648] == 1, subResultReg[1824]== 1, comparatorSig == 0 => should be: result = subResultReg+"100.000"
        assert (output /= ('0' & (1823 downto 0 => '1')))  report "***** Test 6 PASSED  *****" severity note;
        assert (output =  ('0' & (1823 downto 0 => '1')))  report "***** Test 6 FAILURE *****" severity failure;
        
        wait for 50*CLK_PERIOD;
        -- Some number in field. Result should be the same as input.
        assert (output /= ((1824 downto 1000 => '0') & (999 downto 0 => '1')))  report "***** Test 7 PASSED  *****" severity note;
        assert (output =  ((1824 downto 1000 => '0') & (999 downto 0 => '1')))  report "***** Test 7 FAILURE *****" severity failure;
        
        wait for SIM_TIME;
    end process;
    
end Behavioral;
