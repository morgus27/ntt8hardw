----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/01/2016 03:22:04 PM
-- Design Name: 
-- Module Name: MyMagicFSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NTT8Wrapper is
    port ( 
        clk             : IN STD_LOGIC;
        rst             : IN STD_LOGIC;
        enR             : IN STD_LOGIC; --enable read,should be '0' when not in use.
        enW             : IN STD_LOGIC; --enable write,should be '0' when not in use.
        dataOut         : OUT STD_LOGIC_VECTOR(31 downto 0); --output data
        dataIn          : IN STD_LOGIC_VECTOR (31 downto 0); --input data
        empty           : OUT STD_LOGIC; --set as '1' when the queue is empty
        full            : OUT STD_LOGIC
    );
end NTT8Wrapper;


architecture Behavioral of NTT8Wrapper is

     type CALCULATION_STATE is (IDLE, DATA_IN, DATA_OUT, CALCULATING);
     
     signal stateNext, stateReg                  : CALCULATION_STATE;
     signal enableWrite, enableRead              : STD_LOGIC;
     signal resetSig                             : STD_LOGIC;
     
     signal startCalc                            : STD_LOGIC;
     signal calcMode                             : STD_LOGIC_VECTOR(1 downto 0);
     signal calcCounter                          : STD_LOGIC_VECTOR(11 downto 0);
     signal dataMode                             : STD_LOGIC_VECTOR(1 downto 0);
     signal dataCounter                          : STD_LOGIC_VECTOR(6 downto 0);
     
     component InternalFifo is
     port ( 
         clk         : IN STD_LOGIC;
         reset       : IN STD_LOGIC;
         enR         : IN STD_LOGIC;
         enW         : IN STD_LOGIC;
         dataIn      : IN STD_LOGIC_VECTOR (31 downto 0);
         startCalc   : IN STD_LOGIC;
         
         empty       : OUT STD_LOGIC;
         full        : OUT STD_LOGIC;
         dataOut     : OUT STD_LOGIC_VECTOR(31 downto 0)
     );
     end component;
     
     component Counter is
         generic(MEMORY_WORDS_WIDTH: INTEGER);
         port(
             clk         : IN STD_LOGIC;
             rst         : IN STD_LOGIC;
             mode        : IN STD_LOGIC_VECTOR(1 downto 0);
             dataBus     : IN STD_LOGIC_VECTOR(MEMORY_WORDS_WIDTH-1 downto 0);
             output      : OUT STD_LOGIC_VECTOR(MEMORY_WORDS_WIDTH-1 downto 0)
         );
     end component;
         
begin
    
    process(clk, rst)
    begin
        if (rst = '1') then
            stateReg <= IDLE;
        elsif rising_edge(clk) then
            stateReg <= stateNext;    
        end if;
    end process;
    
    process(enR, enW, calcCounter, dataCounter) 
    begin
        case stateReg is           
            when IDLE =>
                if (enW = '1') then
                    enableWrite <= '1';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '0';
                    startCalc <= '0';
                    calcMode <= "00";
                    dataMode <= "10";
                    resetSig <= '0';
                    stateNext   <= DATA_IN;
                else
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '0';
                    startCalc <= '0';
                    calcMode <= "00";
                    dataMode <= "00";
                    resetSig <= '1';
                    stateNext   <= IDLE;                     
                end if;                    
            when DATA_IN =>     
                if (dataCounter = "1010010") then
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '1';
                    startCalc <= '1';
                    calcMode <= "10";
                    dataMode <= "00";
                    resetSig <= '0';
                    stateNext   <= CALCULATING; 
                else 
                    if (enW = '1') then
                        enableWrite <= '1';
                        enableRead  <= '0';
                        empty <= '1';
                        full <= '0';
                        startCalc <= '0';
                        calcMode <= "00";
                        dataMode <= "10";
                        resetSig <= '0';
                        stateNext   <= DATA_IN;
                        
                    else
                        enableWrite <= '0';
                        enableRead  <= '0';
                        empty <= '1';
                        full <= '0';
                        startCalc <= '0';
                        calcMode <= "00";
                        dataMode <= "11";
                        resetSig <= '0';
                        stateNext   <= DATA_IN;       
                    end if;    
                end if;                  
            when CALCULATING =>             
                if (calcCounter = "010000000000") then
                    enableWrite <= '0';         
                    full <= '1';
                    empty <= '0';
                    startCalc <= '1';
                    calcMode <= "11";
                    resetSig <= '0';
                    if (enR='1') then
                        stateNext   <= DATA_OUT;
                        enableRead  <= '1';
                        dataMode <= "10";                      
                    else
                        stateNext   <= CALCULATING;
                        enableRead  <= '0';
                        dataMode <= "00";
                    end if;
                else
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '1';
                    startCalc <= '1';                    
                    calcMode <= "10";
                    dataMode <= "00";
                    resetSig <= '0';
                    stateNext   <= CALCULATING; 
                end if;      
            when DATA_OUT =>
                if (dataCounter = "1010110") then
                    enableWrite <= '0';
                    enableRead  <= '0';
                    empty <= '1';
                    full <= '0';
                    startCalc <= '0';
                    calcMode <= "00";
                    dataMode <= "00";
                    resetSig <= '1';
                    stateNext   <= IDLE; 
                else
                    if (enR='1') then
                        enableWrite <= '0';
                        enableRead  <= '1';
                        empty <= '0';
                        full <= '1';
                        startCalc <= '1';
                        calcMode <= "00";
                        dataMode <= "10";
                        resetSig <= '0';
                        stateNext   <= DATA_OUT; 
                    else
                        enableWrite <= '0';
                        enableRead  <= '0';
                        empty <= '0';
                        full <= '1';
                        startCalc <= '1';
                        calcMode <= "00";
                        dataMode <= "11";
                        resetSig <= '0';
                        stateNext   <= DATA_OUT; 
                    end if;
                end if;
        end case;
    end process;
    
    fifoInst : InternalFifo
    port map ( 
        clk         => clk,
        reset       => resetSig,
        enR         => enableRead,
        enW         => enableWrite,
        dataIn      => dataIn,
        startCalc   => startCalc,

        empty       => open,
        full        => open,
        dataOut     => dataOut        
    );

    calcCounterInst: Counter
    generic map(MEMORY_WORDS_WIDTH => 12)
    port map(
        clk        => clk,
        rst        => resetSig,
        mode       => calcMode,
        dataBus    => "000000000000",
        output     => calcCounter
    );
    
    dataCounterInst: Counter
    generic map(MEMORY_WORDS_WIDTH => 7)
    port map(
        clk        => clk,
        rst        => resetSig,
        mode       => dataMode,
        dataBus    => "0000000",
        output     => dataCounter
    );
     
end Behavioral;
