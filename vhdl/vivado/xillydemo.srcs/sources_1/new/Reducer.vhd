----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 02:23:12 PM
-- Design Name: 
-- Module Name: Adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity Reducer is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        input       : IN STD_LOGIC_VECTOR(3584 downto 0);
        output      : OUT STD_LOGIC_VECTOR(1792 downto 0)
    );
end Reducer;

architecture Behavioral of Reducer is

    signal inputReg                               : STD_LOGIC_VECTOR(3584 downto 0);
    
    signal firstSubSig, secondSubSig,
           firstAddSig, secondAddSig              : STD_LOGIC_VECTOR(2047 downto 0);
    signal subResultReg, subResultSig,
           addResultReg, addResultSig             : STD_LOGIC_VECTOR(2048 downto 0);
    signal resultSig, resultReg                   : STD_LOGIC_VECTOR(1792 downto 0);
           
    signal AeB, AlB, AgB                          : STD_LOGIC;
    
    
    component Subtractor2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;
    
    component Adder2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;
    
    component Comparator1793 is
        port ( A : in STD_LOGIC_VECTOR (1792 downto 0);
               B : in STD_LOGIC_VECTOR (1792 downto 0);
               AgB : out STD_LOGIC;
               AeB : out STD_LOGIC;
               AlB : out STD_LOGIC
       );
    end component;

begin

    redSub: Subtractor2048
    port map (
        clk         => clk,
        rst         => rst,
        
        A           => firstSubSig,
        B           => secondSubSig,
        S           => subResultSig
    );
    
    redAdder: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => firstAddSig,
        B           => secondAddSig,
        S           => addResultSig
    );
    
    redComp: Comparator1793
    port map ( 
           A        => subResultSig(1792 downto 0),
           B        => (1792 downto 0 => '0'),
           AgB      => AgB,
           AeB      => AeB,
           AlB      => AlB
   );

    process (clk, rst)
    begin
        if (rst = '1') then
            inputReg <= (others => '0');
            subResultReg <= (others => '0');
            addResultReg <= (others => '0');
            resultReg <= (others => '0');      
        elsif (rising_edge(clk)) then
            inputReg <= input;
            subResultReg <= subResultSig;     
            addResultReg <= addResultSig;   
            resultReg <= resultSig;                    
        end if;
    end process;
        
    process(inputReg, subResultReg, addResultReg, AeB)
        variable adderCtrl, resultCtrl      : STD_LOGIC_VECTOR(1 downto 0);
    begin
        firstSubSig <= (2047 downto 1792 => '0') & inputReg(1791 downto 0);
        secondSubSig <= (2047 downto 1792 => '0') & inputReg(3583 downto 1792);
        
        firstAddSig <= subResultReg(2047 downto 0);
        adderCtrl := subResultReg(1792) & inputReg(3584);
        case adderCtrl is
            when "10" => secondAddSig <= (2047 downto 1793 => '0') & '1' & (1791 downto 1 => '0') & '1';
            when "11" => secondAddSig <= (2047 downto 1793 => '0') & '1' & (1791 downto 0 => '0');
            when "01" => secondAddSig <= (2047 downto 0 => '1');
            when others => secondAddSig <= (2047 downto 0 => '0');
        end case;  
        
        resultCtrl := inputReg(3584) & AeB;
        case resultCtrl is
            when "01" => resultSig <= (1792 downto 0 => '0');
            when "11" => resultSig <= '1' & (1791 downto 0 => '0');
            when others => resultSig <= addResultReg(1792 downto 0);
        end case;
    end process;
    
    output <= resultReg;
    
end Behavioral;
