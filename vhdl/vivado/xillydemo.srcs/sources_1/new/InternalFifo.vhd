library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity InternalFifo is
    port ( 
        clk         : IN STD_LOGIC;
        reset       : IN STD_LOGIC;
        enR         : IN STD_LOGIC;
        enW         : IN STD_LOGIC;
        dataIn      : IN STD_LOGIC_VECTOR (31 downto 0);
        startCalc   : IN STD_LOGIC;
        
        empty       : OUT STD_LOGIC;
        full        : OUT STD_LOGIC;
        dataOut     : OUT STD_LOGIC_VECTOR(31 downto 0)
    );
end InternalFifo;

architecture Behavioral of InternalFifo is

    type memoryType is array (511 downto 0 ) of STD_LOGIC_VECTOR(31 downto 0);
    
    signal memory               : memoryType;    
    signal readPtr, writePtr    : STD_LOGIC_VECTOR(31 downto 0);
    signal full0                : STD_LOGIC;
    signal empty0               : STD_LOGIC;
    
    signal x1, x2, x3, x4, x5, x6, x7, x8               : STD_LOGIC_VECTOR(1823 downto 0);
    signal fx1, fx2, fx3, fx4, fx5, fx6, fx7, fx8       : STD_LOGIC_VECTOR(1823 downto 0);
    
    component NTT8 is
        port(
            clk    : IN STD_LOGIC;
            rst    : IN STD_LOGIC;
            
            x1     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x2     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x3     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x4     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x5     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x6     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x7     : IN STD_LOGIC_VECTOR(1823 downto 0);
            x8     : IN STD_LOGIC_VECTOR(1823 downto 0);
            
            fx1     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx2     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx3     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx4     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx5     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx6     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx7     : OUT STD_LOGIC_VECTOR(1823 downto 0);
            fx8     : OUT STD_LOGIC_VECTOR(1823 downto 0)
        );
    end component;
    

    begin
        full <= full0;
        empty <= empty0; 
        
        fifo0: process(clk, reset)
        begin
            if reset = '1' then            
                readPtr     <= (others => '0');
                writePtr    <= (others => '0');
                empty0      <='1';
                full0       <='0';
               
            elsif rising_edge(clk) then 
            
                    if (writePtr + '1' = readPtr) then 
                        full0   <=  '1';
                    else
                        full0   <=  '0';
                    end if ;
                    
                    if (readPtr = writePtr) then 
                        empty0  <=  '1';
                    else
                        empty0 <= '0';
                    end if ; 
                                   
                    if (enW = '1') then 
                        memory (conv_integer(writePtr) + 1) <= dataIn ;
                        writePtr <= writePtr + '1' ;
                    end if ; 
                            
                    if (enR = '1') then 
                        if (readPtr = "0000000") then
                            dataOut <= "10101010101010101010101010101010";
                        elsif (readPtr < "000111010") then  --58
                            dataOut <= fx1((conv_integer(readPtr)-1)*32 + 31 downto (conv_integer(readPtr)-1)*32);
                        elsif (readPtr < "001110011") then  --115
                            dataOut <= fx2((conv_integer(readPtr)-58)*32 + 31 downto (conv_integer(readPtr)-58)*32);
                        elsif (readPtr < "010101100") then  --172
                            dataOut <= fx3((conv_integer(readPtr)-115)*32 + 31 downto (conv_integer(readPtr)-115)*32);
                        elsif (readPtr < "011100101") then  --229
                            dataOut <= fx4((conv_integer(readPtr)-172)*32 + 31 downto (conv_integer(readPtr)-172)*32);
                        elsif (readPtr < "100011110") then  --286
                            dataOut <= fx5((conv_integer(readPtr)-229)*32 + 31 downto (conv_integer(readPtr)-229)*32);
                        elsif (readPtr < "101010111") then  --343
                            dataOut <= fx6((conv_integer(readPtr)-286)*32 + 31 downto (conv_integer(readPtr)-286)*32);
                        elsif (readPtr < "110010000") then  --400
                            dataOut <= fx7((conv_integer(readPtr)-343)*32 + 31 downto (conv_integer(readPtr)-343)*32);
                        elsif (readPtr < "111001001") then  --457
                            dataOut <= fx8((conv_integer(readPtr)-400)*32 + 31 downto (conv_integer(readPtr)-400)*32);
                        else 
                            dataOut <= memory(conv_integer(readPtr)-1);
                        end if;
                        readPtr <= readPtr + '1' ;
                    end if ; 
                end if;
            
        end process;
        
        x1 <=                                            memory(57) & memory(56) & memory(55) & memory(54) & memory(53) & memory(52) & memory(51)
                & memory(50) & memory(49) & memory(48) & memory(47) & memory(46) & memory(45) & memory(44) & memory(43) & memory(42) & memory(41)
                & memory(40) & memory(39) & memory(38) & memory(37) & memory(36) & memory(35) & memory(34) & memory(33) & memory(32) & memory(31)
                & memory(30) & memory(29) & memory(28) & memory(27) & memory(26) & memory(25) & memory(24) & memory(23) & memory(22) & memory(21)
                & memory(20) & memory(19) & memory(18) & memory(17) & memory(16) & memory(15) & memory(14) & memory(13) & memory(12) & memory(11) 
                & memory(10) & memory(9) & memory(8) & memory(7) & memory(6) & memory(5) & memory(4) & memory(3) & memory(2) & memory(1);
        x2 <=                                                                                         memory(114) & memory(113) & memory(112) & memory(111)
                & memory(110) & memory(109) & memory(108) & memory(107) & memory(106) & memory(105) & memory(104) & memory(103) & memory(102) & memory(101)
                & memory(100) & memory(99) & memory(98) & memory(97) & memory(96) & memory(95) & memory(94) & memory(93) & memory(92) & memory(91)
                & memory(90) & memory(89) & memory(88) & memory(87) & memory(86) & memory(85) & memory(84) & memory(83) & memory(82) & memory(81)
                & memory(80) & memory(79) & memory(78) & memory(77) & memory(76) & memory(75) & memory(74) & memory(73) & memory(72) & memory(71)
                & memory(70) & memory(69) & memory(68) & memory(67) & memory(66) & memory(65) & memory(64) & memory(63) & memory(62) & memory(61) 
                & memory(60) & memory(59) & memory(58);
                
        x3 <=                                                                                                                                   memory(171)
                & memory(170) & memory(169) & memory(168) & memory(167) & memory(166) & memory(165) & memory(164) & memory(163) & memory(162) & memory(161)
                & memory(160) & memory(159) & memory(158) & memory(157) & memory(156) & memory(155) & memory(154) & memory(153) & memory(152) & memory(151)
                & memory(150) & memory(149) & memory(148) & memory(147) & memory(146) & memory(145) & memory(144) & memory(143) & memory(142) & memory(141)
                & memory(140) & memory(139) & memory(138) & memory(137) & memory(136) & memory(135) & memory(134) & memory(133) & memory(132) & memory(131)
                & memory(130) & memory(129) & memory(128) & memory(127) & memory(126) & memory(125) & memory(124) & memory(123) & memory(122) & memory(121)
                & memory(120) & memory(119) & memory(118) & memory(117) & memory(116) & memory(115);
                
        x4 <=                                 memory(228) & memory(227) & memory(226) & memory(225) & memory(224) & memory(223) & memory(222) & memory(221)
                & memory(220) & memory(219) & memory(218) & memory(217) & memory(216) & memory(215) & memory(214) & memory(213) & memory(212) & memory(211)
                & memory(210) & memory(209) & memory(208) & memory(207) & memory(206) & memory(205) & memory(204) & memory(203) & memory(202) & memory(201)
                & memory(200) & memory(199) & memory(198) & memory(197) & memory(196) & memory(195) & memory(194) & memory(193) & memory(192) & memory(191)
                & memory(190) & memory(189) & memory(188) & memory(187) & memory(186) & memory(185) & memory(184) & memory(183) & memory(182) & memory(181)
                & memory(180) & memory(179) & memory(178) & memory(177) & memory(176) & memory(175) & memory(174) & memory(173) & memory(172);
                
        x5 <=                                                                           memory(285) & memory(284) & memory(283) & memory(282) & memory(281)
                & memory(280) & memory(279) & memory(278) & memory(277) & memory(276) & memory(275) & memory(274) & memory(273) & memory(272) & memory(271)
                & memory(270) & memory(269) & memory(268) & memory(267) & memory(266) & memory(265) & memory(264) & memory(263) & memory(262) & memory(261)
                & memory(260) & memory(259) & memory(258) & memory(257) & memory(256) & memory(255) & memory(254) & memory(253) & memory(252) & memory(251)
                & memory(250) & memory(249) & memory(248) & memory(247) & memory(246) & memory(245) & memory(244) & memory(243) & memory(242) & memory(241)
                & memory(240) & memory(239) & memory(238) & memory(237) & memory(236) & memory(235) & memory(234) & memory(233) & memory(232) & memory(231)
                & memory(230) & memory(229);
                
        x6 <=                                                                                                                     memory(342) & memory(341)
                & memory(340) & memory(339) & memory(338) & memory(337) & memory(336) & memory(335) & memory(334) & memory(333) & memory(332) & memory(331)
                & memory(330) & memory(329) & memory(328) & memory(327) & memory(326) & memory(325) & memory(324) & memory(323) & memory(322) & memory(321)
                & memory(320) & memory(319) & memory(318) & memory(317) & memory(316) & memory(315) & memory(314) & memory(313) & memory(312) & memory(311)
                & memory(310) & memory(309) & memory(308) & memory(307) & memory(306) & memory(305) & memory(304) & memory(303) & memory(302) & memory(301)
                & memory(300) & memory(299) & memory(298) & memory(297) & memory(296) & memory(295) & memory(294) & memory(293) & memory(292) & memory(291)
                & memory(290) & memory(289) & memory(288) & memory(287) & memory(286);
                
        x7 <=                   memory(399) & memory(398) & memory(397) & memory(396) & memory(395) & memory(394) & memory(393) & memory(392) & memory(391)
                & memory(390) & memory(389) & memory(388) & memory(387) & memory(386) & memory(385) & memory(384) & memory(383) & memory(382) & memory(381)
                & memory(380) & memory(379) & memory(378) & memory(377) & memory(376) & memory(375) & memory(374) & memory(373) & memory(372) & memory(371)
                & memory(370) & memory(369) & memory(368) & memory(367) & memory(366) & memory(365) & memory(364) & memory(363) & memory(362) & memory(361)
                & memory(360) & memory(359) & memory(358) & memory(357) & memory(356) & memory(355) & memory(354) & memory(353) & memory(352) & memory(351)
                & memory(350) & memory(349) & memory(348) & memory(347) & memory(346) & memory(345) & memory(344) & memory(343);
                
        x8 <=                                                             memory(456) & memory(455) & memory(454) & memory(453) & memory(452) & memory(451)
                & memory(450) & memory(449) & memory(448) & memory(447) & memory(446) & memory(445) & memory(444) & memory(443) & memory(442) & memory(441)
                & memory(440) & memory(439) & memory(438) & memory(437) & memory(436) & memory(435) & memory(434) & memory(433) & memory(432) & memory(431)
                & memory(430) & memory(429) & memory(428) & memory(427) & memory(426) & memory(425) & memory(424) & memory(423) & memory(422) & memory(421)
                & memory(420) & memory(419) & memory(418) & memory(417) & memory(416) & memory(415) & memory(414) & memory(413) & memory(412) & memory(411)
                & memory(410) & memory(409) & memory(408) & memory(407) & memory(406) & memory(405) & memory(404) & memory(403) & memory(402) & memory(401)
                & memory(400);
                
                
        ntt8Inst: NTT8
        port map(
            clk    => clk,
            rst    => reset,
            
            x1     => x1,
            x2     => x2,
            x3     => x3,
            x4     => x4,
            x5     => x5,
            x6     => x6,
            x7     => x7,
            x8     => x8,
            
            fx1    => fx1,
            fx2    => fx2,
            fx3    => fx3,
            fx4    => fx4,
            fx5    => fx5,
            fx6    => fx6,
            fx7    => fx7,
            fx8    => fx8
        );   
        
end Behavioral;