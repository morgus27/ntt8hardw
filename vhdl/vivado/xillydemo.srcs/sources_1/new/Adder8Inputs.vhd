----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 04:02:33 PM
-- Design Name: 
-- Module Name: F2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Adder8Inputs is
    generic(INPUT_LENGTH: INTEGER := 1825);
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        A           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        B           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        C           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        D           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        E           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        F           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        G           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        H           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(2048  downto 0)
    );
end Adder8Inputs;

architecture Behavioral of Adder8Inputs is

    signal AReg, BReg, 
           CReg, DReg,
           EReg, FReg,
           GReg, HReg                : STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
    
    signal adderF01Sig, adderF01Reg,
           adderF02Sig, adderF02Reg,
           adderF03Sig, adderF03Reg,
           adderF04Sig, adderF04Reg              : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal adderF05Sig, adderF05Reg,
           adderF06Sig, adderF06Reg              : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal adderF07Sig, adderF07Reg              : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal ASig, BSig,
           CSig, DSig,
           ESig, FSig,
           GSig, HSig                : STD_LOGIC_VECTOR(2047 downto 0);
    
    component Adder2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;

begin

    adderF01: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => ASig,
        B           => BSig,
        S           => adderF01Sig
    );
    
    adderF02: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => CSig,
        B           => DSig,
        S           => adderF02Sig
    );
    
    adderF03: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => ESig,
        B           => FSig,
        S           => adderF03Sig
    );
    
    adderF04: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => GSig,
        B           => HSig,
        S           => adderF04Sig
    );
    
    adderF05: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => adderF01Reg(2047 downto 0),
        B           => adderF02Reg(2047 downto 0),
        S           => adderF05Sig
    );
        
    adderF06: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => adderF03Reg(2047 downto 0),
        B           => adderF04Reg(2047 downto 0),
        S           => adderF06Sig
    ); 
    
    adderF07: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => adderF05Reg(2047 downto 0),
        B           => adderF06Reg(2047 downto 0),
        S           => adderF07Sig
    ); 
     
        
    process (clk, rst)
    begin
        if (rst = '1') then
            AReg <= (others => '0');
            BReg <= (others => '0');
            CReg <= (others => '0');
            DReg <= (others => '0');
            EReg <= (others => '0');
            FReg <= (others => '0');
            GReg <= (others => '0');
            HReg <= (others => '0');
            
            adderF01Reg <= (others => '0');
            adderF02Reg <= (others => '0');
            adderF03Reg <= (others => '0');
            adderF04Reg <= (others => '0');
            adderF05Reg <= (others => '0');
            adderF06Reg <= (others => '0');
            adderF07Reg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            AReg <= A;
            BReg <= B;
            CReg <= C;
            DReg <= D;
            EReg <= E;
            FReg <= F;
            GReg <= G;
            HReg <= H;
            
            adderF01Reg <= adderF01Sig;
            adderF02Reg <= adderF02Sig;
            adderF03Reg <= adderF03Sig;
            adderF04Reg <= adderF04Sig;
            adderF05Reg <= adderF05Sig;
            adderF06Reg <= adderF06Sig;
            adderF07Reg <= adderF07Sig;
                                                                     
        end if;
    end process;
        
    process(AReg, BReg, CReg, DReg, EReg, FReg, GReg, HReg)
    begin
   
        ASig <= (2047 downto INPUT_LENGTH => '0') & AReg;
        BSig <= (2047 downto INPUT_LENGTH => '0') & BReg;
        CSig <= (2047 downto INPUT_LENGTH => '0') & CReg;
        DSig <= (2047 downto INPUT_LENGTH => '0') & DReg;                                                               
        ESig <= (2047 downto INPUT_LENGTH => '0') & EReg;
        FSig <= (2047 downto INPUT_LENGTH => '0') & FReg;
        GSig <= (2047 downto INPUT_LENGTH => '0') & GReg;
        HSig <= (2047 downto INPUT_LENGTH => '0') & HReg;
        
    end process;
    
    result <= adderF07Reg;
    
end Behavioral;
