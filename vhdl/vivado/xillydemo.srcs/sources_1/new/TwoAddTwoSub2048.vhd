----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/12/2016 11:44:59 AM
-- Design Name: 
-- Module Name: Adder4Inputs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity TwoAddTwoSub2048 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        A           : IN STD_LOGIC_VECTOR(2047 downto 0);
        B           : IN STD_LOGIC_VECTOR(2047 downto 0);
        C           : IN STD_LOGIC_VECTOR(2047 downto 0);
        D           : IN STD_LOGIC_VECTOR(2047 downto 0);        
        
        result      : OUT STD_LOGIC_VECTOR(2048  downto 0)
    );
end TwoAddTwoSub2048;

architecture Behavioral of TwoAddTwoSub2048 is

    signal AReg, BReg, 
           CReg, DReg                            : STD_LOGIC_VECTOR(2047 downto 0);
    
    signal subtractor1Sig, subtractor1Reg,
           subtractor2Sig, subtractor2Reg        : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal adder3Sig, adder3Reg                  : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal ASig, BSig,
           CSig, DSig                            : STD_LOGIC_VECTOR(2047 downto 0);
    
    component Adder2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;
    
    component Subtractor2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;

begin

    subtractorF01: Subtractor2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => AReg,
        B           => CReg,
        S           => subtractor1Sig
    );
    
    subtractorF02: Subtractor2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => BReg,
        B           => DReg,
        S           => subtractor2Sig
    );
    
    
    adderF03: Adder2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => subtractor1Reg(2047 downto 0),
        B           => subtractor2Reg(2047 downto 0),
        S           => adder3Sig
    );
     
        
    process (clk, rst)
    begin
        if (rst = '1') then
            AReg <= (others => '0');
            BReg <= (others => '0');
            CReg <= (others => '0');
            DReg <= (others => '0');
            
            subtractor1Reg <= (others => '0');
            subtractor2Reg <= (others => '0');
            adder3Reg <= (others => '0');
        elsif (rising_edge(clk)) then
            AReg <= A;
            BReg <= B;
            CReg <= C;
            DReg <= D;
            
            subtractor1Reg <= subtractor1Sig;
            subtractor2Reg <= subtractor2Sig;
            adder3Reg <= adder3Sig;
                                                                     
        end if;
    end process;
        
    result <= adder3Reg;
    
end Behavioral;
