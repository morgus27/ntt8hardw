----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/19/2016 10:17:35 PM
-- Design Name: 
-- Module Name: Adder4Inputs3072 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Adder4Inputs3072 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        A           : IN STD_LOGIC_VECTOR(3071 downto 0);
        B           : IN STD_LOGIC_VECTOR(3071 downto 0);
        C           : IN STD_LOGIC_VECTOR(3071 downto 0);
        D           : IN STD_LOGIC_VECTOR(3071 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(3072  downto 0)
    );
end Adder4Inputs3072;

architecture Behavioral of Adder4Inputs3072 is

    component Adder3072 is
    port (
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        A           : IN STD_LOGIC_VECTOR(3071 downto 0);
        B           : IN STD_LOGIC_VECTOR(3071 downto 0);
        S           : OUT STD_LOGIC_VECTOR(3072 downto 0)
    );
    end component;


    signal AReg, BReg, 
           CReg, DReg                            : STD_LOGIC_VECTOR(3071 downto 0);
    
    signal adderF01Sig, adderF01Reg,
           adderF02Sig, adderF02Reg,
           adderF11Sig, adderF11Reg              : STD_LOGIC_VECTOR(3072 downto 0);
    
    begin
    
    adderF01: Adder3072
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => AReg,
        B           => BReg,
        S           => adderF01Sig
    );
    
    adderF02: Adder3072
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => CReg,
        B           => DReg,
        S           => adderF02Sig
    );
    
    adderF11: Adder3072
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => adderF01Reg(3071 downto 0),
        B           => adderF02Reg(3071 downto 0),
        S           => adderF11Sig
    );
        
    process (clk, rst)
    begin
        if (rst = '1') then
            AReg <= (others => '0');
            BReg <= (others => '0');
            CReg <= (others => '0');
            DReg <= (others => '0');
            
            adderF01Reg <= (others => '0');
            adderF02Reg <= (others => '0');
            adderF02Reg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            AReg <= A;
            BReg <= B;
            CReg <= C;
            DReg <= D;

            adderF01Reg <= adderF01Sig;
            adderF02Reg <= adderF02Sig;
            adderF11Reg <= adderF11Sig;
                                                                     
        end if;
    end process;       
    
    result <= adderF11Reg;

end Behavioral;
