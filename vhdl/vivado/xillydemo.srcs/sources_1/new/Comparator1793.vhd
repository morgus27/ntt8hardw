----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/17/2016 10:26:42 AM
-- Design Name: 
-- Module Name: Comparator1793 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Comparator1793 is
    port ( A : in STD_LOGIC_VECTOR (1792 downto 0);
           B : in STD_LOGIC_VECTOR (1792 downto 0);
           AgB : out STD_LOGIC;
           AeB : out STD_LOGIC;
           AlB : out STD_LOGIC);
end Comparator1793;

architecture Behavioral of Comparator1793 is


begin

    process(A,B)
    begin
        if (A < B) then
            AgB <= '0';
            AeB <= '0';
            AlB <= '1';
        elsif (A = B) then
            AgB <= '0';
            AeB <= '1';
            AlB <= '0';
        else 
            AgB <= '1';
            AeB <= '0';
            AlB <= '0';
        end if;
    end process;

end Behavioral;
