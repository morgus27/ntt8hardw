----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/13/2016 05:37:49 PM
-- Design Name: 
-- Module Name: Adder2048 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Subtractor2048 is
    port (
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        A           : IN STD_LOGIC_VECTOR(2047 downto 0);
        B           : IN STD_LOGIC_VECTOR(2047 downto 0);
        S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
    );
end Subtractor2048;

architecture Behavioral of Subtractor2048 is

    signal AReg, BReg                                       : STD_LOGIC_VECTOR(2047 downto 0);
    signal resultSig, resultReg                             : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal subtractor1Sig, subtractor2Sig, subtractor3Sig, subtractor4Sig,
           subtractor5Sig, subtractor6Sig, subtractor7Sig, subtractor8Sig,
           subtractor1Reg, subtractor2Reg, subtractor3Reg, subtractor4Reg,
           subtractor5Reg, subtractor6Reg, subtractor7Reg, subtractor8Reg,
           subtractor1CSig, subtractor2CSig, subtractor3CSig, subtractor4CSig,
           subtractor5CSig, subtractor6CSig, subtractor7CSig, subtractor8CSig,
           subtractor1CReg, subtractor2CReg, subtractor3CReg, subtractor4CReg,
           subtractor5CReg, subtractor6CReg, subtractor7CReg, subtractor8CReg   : STD_LOGIC_VECTOR(256 downto 0);
    
    signal subtractor1CInp, subtractor2CInp, subtractor3CInp, subtractor4CInp,   
           subtractor5CInp, subtractor6CInp, subtractor7CInp, subtractor8CInp   : STD_LOGIC_VECTOR(255 downto 0);
    
    component c_addsub_0 is
        port(
           A         : IN STD_LOGIC_VECTOR(255 downto 0);
           B         : IN STD_LOGIC_VECTOR(255 downto 0);
           
           ADD       : IN STD_LOGIC;
           CLK       : IN STD_LOGIC; 
           SCLR      : IN STD_LOGIC;
           
           S         : OUT STD_LOGIC_VECTOR(256 downto 0)
        );
    end component;

begin

    subtractor1: c_addsub_0
    port map(
        A       => AReg(255 downto 0),
        B       => BReg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor1Sig
    );
    
    subtractor2: c_addsub_0
    port map(
        A       => AReg(511 downto 256),
        B       => BReg(511 Downto 256),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor2Sig
    );
    
    subtractor3: c_addsub_0
    port map(
        A       => AReg(767 downto 512),
        B       => BReg(767 Downto 512),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor3Sig
    );
    
    subtractor4: c_addsub_0
    port map(
        A       => AReg(1023 downto 768),
        B       => BReg(1023 Downto 768),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor4Sig
    );
    
    subtractor5: c_addsub_0
    port map(
        A       => AReg(1279 downto 1024),
        B       => BReg(1279 Downto 1024),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor5Sig
    );
    
    subtractor6: c_addsub_0
    port map(
        A       => AReg(1535 downto 1280),
        B       => BReg(1535 Downto 1280),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor6Sig
    );
    
    subtractor7: c_addsub_0
    port map(
        A       => AReg(1791 downto 1536),
        B       => BReg(1791 Downto 1536),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor7Sig
    );
    
    subtractor8: c_addsub_0
    port map(
        A       => AReg(2047 downto 1792),
        B       => BReg(2047 Downto 1792),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor8Sig
    );
    
    subtractor1C: c_addsub_0
    port map(
        B       => subtractor1CInp,
        A       => subtractor2Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor1CSig
    );
    
    subtractor2C: c_addsub_0
    port map(
        B       => subtractor2CInp,
        A       => subtractor3Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor2CSig
    );
    
    subtractor3C: c_addsub_0
    port map(
        B       => subtractor3CInp,
        A       => subtractor4Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor3CSig
    );
    
    subtractor4C: c_addsub_0
    port map(
        B       => subtractor4CInp,
        A       => subtractor5Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor4CSig
    );
    
    subtractor5C: c_addsub_0
    port map(
        B       => subtractor5CInp,
        A       => subtractor6Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor5CSig
    );
    
    subtractor6C: c_addsub_0
    port map(
        B       => subtractor6CInp,
        A       => subtractor7Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor6CSig
    );
    
    subtractor7C: c_addsub_0
    port map(
        B       => subtractor7CInp,
        A       => subtractor8Reg(255 Downto 0),
        
        ADD     => '0',
        CLK     => clk,
        SCLR    => rst,
        S       => subtractor7CSig
    );
    
    process (clk, rst)
    begin
        if (rst = '1') then
            AReg <= (others => '0');
            BReg <= (others => '0');
            
            subtractor1Reg <= (others => '0');
            subtractor2Reg <= (others => '0');
            subtractor3Reg <= (others => '0');
            subtractor4Reg <= (others => '0');
            subtractor5Reg <= (others => '0');
            subtractor6Reg <= (others => '0');
            subtractor7Reg <= (others => '0');
            subtractor8Reg <= (others => '0');
            
            subtractor1CReg <= (others => '0');
            subtractor2CReg <= (others => '0');
            subtractor3CReg <= (others => '0');
            subtractor4CReg <= (others => '0');
            subtractor5CReg <= (others => '0');
            subtractor6CReg <= (others => '0');
            subtractor7CReg <= (others => '0');
            
            resultReg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            AReg <= A;
            BReg <= B;
            
            subtractor1Reg <= subtractor1Sig;
            subtractor2Reg <= subtractor2Sig;
            subtractor3Reg <= subtractor3Sig;
            subtractor4Reg <= subtractor4Sig;
            subtractor5Reg <= subtractor5Sig;
            subtractor6Reg <= subtractor6Sig;
            subtractor7Reg <= subtractor7Sig;
            subtractor8Reg <= subtractor8Sig;
            
            subtractor1CReg <= subtractor1CSig;
            subtractor2CReg <= subtractor2CSig;
            subtractor3CReg <= subtractor3CSig;
            subtractor4CReg <= subtractor4CSig;
            subtractor5CReg <= subtractor5CSig;
            subtractor6CReg <= subtractor6CSig;
            subtractor7CReg <= subtractor7CSig;
            
            resultReg <= resultSig;
                                                                     
        end if;
    end process;
    
    process(subtractor1Reg, subtractor2Reg, subtractor3Reg, subtractor4Reg, subtractor5Reg, subtractor6Reg, subtractor7Reg, subtractor8Reg,
            subtractor1CReg, subtractor2CReg, subtractor3CReg, subtractor4CReg, subtractor5CReg, subtractor6CReg, subtractor7CReg)
    begin
        subtractor1CInp <= (254 downto 0 => '0') & subtractor1Reg(256);
        subtractor2CInp <= (254 downto 0 => '0') & (subtractor2Reg(256) or subtractor1CReg(256));
        subtractor3CInp <= (254 downto 0 => '0') & (subtractor3Reg(256) or subtractor2CReg(256));
        subtractor4CInp <= (254 downto 0 => '0') & (subtractor4Reg(256) or subtractor3CReg(256));
        subtractor5CInp <= (254 downto 0 => '0') & (subtractor5Reg(256) or subtractor4CReg(256));
        subtractor6CInp <= (254 downto 0 => '0') & (subtractor6Reg(256) or subtractor5CReg(256));
        subtractor7CInp <= (254 downto 0 => '0') & (subtractor7Reg(256) or subtractor6CReg(256));
        
        resultSig <= (subtractor8Reg(256) or subtractor7CReg(256)) & subtractor7CReg(255 downto 0)
                     & subtractor6CReg(255 downto 0) & subtractor5CReg(255 downto 0) & subtractor4CReg(255 downto 0)
                     & subtractor3CReg(255 downto 0) & subtractor2CReg(255 downto 0) & subtractor1CReg(255 downto 0)
                     & subtractor1Reg(255 downto 0);
    end process;
    
    S <= resultReg;

end Behavioral;
