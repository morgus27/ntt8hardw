----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/13/2016 05:37:49 PM
-- Design Name: 
-- Module Name: Adder2048 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder2048 is
    port (
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        A           : IN STD_LOGIC_VECTOR(2047 downto 0);
        B           : IN STD_LOGIC_VECTOR(2047 downto 0);
        S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
    );
end Adder2048;

architecture Behavioral of Adder2048 is

    signal AReg, BReg                                       : STD_LOGIC_VECTOR(2047 downto 0);
    signal resultSig, resultReg                             : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal adder1Sig, adder2Sig, adder3Sig, adder4Sig       : STD_LOGIC_VECTOR(256 downto 0);
    signal adder5Sig, adder6Sig, adder7Sig, adder8Sig       : STD_LOGIC_VECTOR(256 downto 0);
    signal adder1Reg, adder2Reg, adder3Reg, adder4Reg       : STD_LOGIC_VECTOR(256 downto 0);
    signal adder5Reg, adder6Reg, adder7Reg, adder8Reg       : STD_LOGIC_VECTOR(256 downto 0);
    
    signal adder1CInp, adder2CInp, adder3CInp, adder4CInp   : STD_LOGIC_VECTOR(255 downto 0);
    signal adder5CInp, adder6CInp, adder7CInp               : STD_LOGIC_VECTOR(255 downto 0);
    
    signal adder1CSig, adder2CSig, adder3CSig, adder4CSig   : STD_LOGIC_VECTOR(256 downto 0);
    signal adder5CSig, adder6CSig, adder7CSig               : STD_LOGIC_VECTOR(256 downto 0);
    signal adder1CReg, adder2CReg, adder3CReg, adder4CReg   : STD_LOGIC_VECTOR(256 downto 0);
    signal adder5CReg, adder6CReg, adder7CReg               : STD_LOGIC_VECTOR(256 downto 0);
    
    component c_addsub_0 is
        port(
           A         : IN STD_LOGIC_VECTOR(255 downto 0);
           B         : IN STD_LOGIC_VECTOR(255 downto 0);
           
           ADD       : IN STD_LOGIC;
           CLK       : IN STD_LOGIC; 
           SCLR      : IN STD_LOGIC;
           S         : OUT STD_LOGIC_VECTOR(256 downto 0)
        );
    end component;

begin

    adder1: c_addsub_0
    port map(
        A       => AReg(255 downto 0),
        B       => BReg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder1Sig
    );
    
    adder2: c_addsub_0
    port map(
        A       => AReg(511 downto 256),
        B       => BReg(511 Downto 256),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder2Sig
    );
    
    adder3: c_addsub_0
    port map(
        A       => AReg(767 downto 512),
        B       => BReg(767 Downto 512),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder3Sig
    );
    
    adder4: c_addsub_0
    port map(
        A       => AReg(1023 downto 768),
        B       => BReg(1023 Downto 768),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder4Sig
    );
    
    adder5: c_addsub_0
    port map(
        A       => AReg(1279 downto 1024),
        B       => BReg(1279 Downto 1024),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder5Sig
    );
    
    adder6: c_addsub_0
    port map(
        A       => AReg(1535 downto 1280),
        B       => BReg(1535 Downto 1280),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder6Sig
    );
    
    adder7: c_addsub_0
    port map(
        A       => AReg(1791 downto 1536),
        B       => BReg(1791 Downto 1536),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder7Sig
    );
    
    adder8: c_addsub_0
    port map(
        A       => AReg(2047 downto 1792),
        B       => BReg(2047 Downto 1792),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder8Sig
    );
    
    adder1C: c_addsub_0
    port map(
        A       => adder1CInp,
        B       => adder2Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder1CSig
    );
    
    adder2C: c_addsub_0
    port map(
        A       => adder2CInp,
        B       => adder3Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder2CSig
    );
    
    adder3C: c_addsub_0
    port map(
        A       => adder3CInp,
        B       => adder4Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder3CSig
    );
    
    adder4C: c_addsub_0
    port map(
        A       => adder4CInp,
        B       => adder5Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder4CSig
    );
    
    adder5C: c_addsub_0
    port map(
        A       => adder5CInp,
        B       => adder6Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder5CSig
    );
    
    adder6C: c_addsub_0
    port map(
        A       => adder6CInp,
        B       => adder7Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder6CSig
    );
    
    adder7C: c_addsub_0
    port map(
        A       => adder7CInp,
        B       => adder8Reg(255 Downto 0),
        
        ADD     => '1',
        CLK     => clk,
        SCLR    => rst,
        S       => adder7CSig
    );
    
    process (clk, rst)
    begin
        if (rst = '1') then
            AReg <= (others => '0');
            BReg <= (others => '0');
            
            adder1Reg <= (others => '0');
            adder2Reg <= (others => '0');
            adder3Reg <= (others => '0');
            adder4Reg <= (others => '0');
            adder5Reg <= (others => '0');
            adder6Reg <= (others => '0');
            adder7Reg <= (others => '0');
            adder8Reg <= (others => '0');
            
            adder1CReg <= (others => '0');
            adder2CReg <= (others => '0');
            adder3CReg <= (others => '0');
            adder4CReg <= (others => '0');
            adder5CReg <= (others => '0');
            adder6CReg <= (others => '0');
            adder7CReg <= (others => '0');
            
            resultReg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            AReg <=  A;
            BReg <= B;
            
            adder1Reg <= adder1Sig;
            adder2Reg <= adder2Sig;
            adder3Reg <= adder3Sig;
            adder4Reg <= adder4Sig;
            adder5Reg <= adder5Sig;
            adder6Reg <= adder6Sig;
            adder7Reg <= adder7Sig;
            adder8Reg <= adder8Sig;
            
            adder1CReg <= adder1CSig;
            adder2CReg <= adder2CSig;
            adder3CReg <= adder3CSig;
            adder4CReg <= adder4CSig;
            adder5CReg <= adder5CSig;
            adder6CReg <= adder6CSig;
            adder7CReg <= adder7CSig;
            
            resultReg <= resultSig;
                                                                     
        end if;
    end process;
    
    process(adder1Reg, adder2Reg, adder3Reg, adder4Reg, adder5Reg, adder6Reg, adder7Reg, adder8Reg,
            adder1CReg, adder2CReg, adder3CReg, adder4CReg, adder5CReg, adder6CReg, adder7CReg)
    begin
        adder1CInp <= (254 downto 0 => '0') & adder1Reg(256);
        adder2CInp <= (254 downto 0 => '0') & (adder2Reg(256) or adder1CReg(256));
        adder3CInp <= (254 downto 0 => '0') & (adder3Reg(256) or adder2CReg(256));
        adder4CInp <= (254 downto 0 => '0') & (adder4Reg(256) or adder3CReg(256));
        adder5CInp <= (254 downto 0 => '0') & (adder5Reg(256) or adder4CReg(256));
        adder6CInp <= (254 downto 0 => '0') & (adder6Reg(256) or adder5CReg(256));
        adder7CInp <= (254 downto 0 => '0') & (adder7Reg(256) or adder6CReg(256));
        
        resultSig <= (adder8Reg(256) or adder7CReg(256)) & adder7CReg(255 downto 0) & adder6CReg(255 downto 0)
                     & adder5CReg(255 downto 0) & adder4CReg(255 downto 0) & adder3CReg(255 downto 0)
                     & adder2CReg(255 downto 0) & adder1CReg(255 downto 0) & adder1Reg(255 downto 0);
    end process;
    
    S <= resultReg;

end Behavioral;
