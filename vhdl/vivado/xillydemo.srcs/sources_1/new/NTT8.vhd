----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 02:56:49 PM
-- Design Name: 
-- Module Name: NTT8 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity NTT8 is
    port(
        clk    : IN STD_LOGIC;
        rst    : IN STD_LOGIC;
        
        x1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        x8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        fx1     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx2     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx3     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx4     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx5     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx6     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx7     : OUT STD_LOGIC_VECTOR(1823 downto 0);
        fx8     : OUT STD_LOGIC_VECTOR(1823 downto 0)
    );
end NTT8;

architecture Behavioral of NTT8 is

    component F0 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F1 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F2 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F3 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F4 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F5 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F6 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;
    
    component F7 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
    end component;

begin

    f0Inst: F0 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx1
    );
    
    f1Inst: F1 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx5
    );
        
    f2Inst: F2 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx3
    );
            
    f3Inst: F3 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx7
    );
                
    f4Inst: F4 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx2
    );
                                  
    f5Inst: F5 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx6
    );
                        
    f6Inst: F6 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx4
    );
                            
    f7Inst: F7 
    port map(
        clk         => clk,
        rst         => rst,
        
        f1          => x1,
        f2          => x2,
        f3          => x3,
        f4          => x4,
        f5          => x5,
        f6          => x6,
        f7          => x7,
        f8          => x8,
        
        result      => fx8
    );

end Behavioral;
