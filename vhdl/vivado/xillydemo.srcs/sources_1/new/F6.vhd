----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 06:14:58 PM
-- Design Name: 
-- Module Name: F0 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 03:44:47 PM
-- Design Name: 
-- Module Name: Adder8Inputs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity F6 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
end F6;

architecture Behavioral of F6 is

    signal f1Reg, f2Reg, f3Reg, f4Reg, f5Reg, f6Reg, f7Reg, f8Reg   : STD_LOGIC_VECTOR(1823 downto 0);
    signal f1SigInp, f2SigInp, f3SigInp, f4SigInp, 
           f5SigInp, f6SigInp, f7SigInp, f8SigInp                   : STD_LOGIC_VECTOR(2047 downto 0);
    signal tats1Sig, tats2Sig,
           tats1Reg, tats2Reg                                       : STD_LOGIC_VECTOR(2048 downto 0);
    signal adder1Inp, adder2Inp                                     : STD_LOGIC_VECTOR(2047 downto 0);
    signal adderResultSig, adderResultReg                           : STD_LOGIC_VECTOR(2048 downto 0);
    signal redInput                                                 : STD_LOGIC_VECTOR(3584 downto 0);
    signal resultReg, resultSig                                     : STD_LOGIC_VECTOR(1792 downto 0);
    
    component Reducer is
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            input       : IN STD_LOGIC_VECTOR(3584 downto 0);
            output      : OUT STD_LOGIC_VECTOR(1792 downto 0)
        );
    end component;
       
    component TwoAddTwoSub2048 is
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            C           : IN STD_LOGIC_VECTOR(2047 downto 0);
            D           : IN STD_LOGIC_VECTOR(2047 downto 0);        
            
            result      : OUT STD_LOGIC_VECTOR(2048  downto 0)
        );
    end component;
    
    component Adder2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;

begin


    tats1: TwoAddTwoSub2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => f1SigInp,
        B           => f5SigInp,
        C           => f3SigInp,
        D           => f7SigInp,        
        
        result      => tats1Sig
    );
    
    tats2: TwoAddTwoSub2048
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => f4SigInp,
        B           => f8SigInp,
        C           => f2SigInp,
        D           => f6SigInp,        
        
        result      => tats2Sig
    );
    
    finalAdder: Adder2048
    port map (
        clk         => clk,
        rst         => rst,
        
        A           => adder1Inp,
        B           => adder2Inp,
        S           => adderResultSig
    );
    
    reducerF0: Reducer
    port map(
       clk         => clk,
       rst         => rst,
       
       input       => redInput,  
       output      => resultSig
    );

    process (clk, rst)
    begin
        if (rst = '1') then
            f1Reg <= (others => '0');
            f2Reg <= (others => '0');
            f3Reg <= (others => '0');
            f4Reg <= (others => '0');
            f5Reg <= (others => '0');
            f6Reg <= (others => '0');
            f7Reg <= (others => '0');
            f8Reg <= (others => '0');
            tats1Reg  <= (others => '0');
            tats2Reg  <= (others => '0');
            adderResultReg <= (others => '0');
            resultReg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            f1Reg <= f1;
            f2Reg <= f2;
            f3Reg <= f3;
            f4Reg <= f4;
            f5Reg <= f5;
            f6Reg <= f6;
            f7Reg <= f7;
            f8Reg <= f8;
            tats1Reg  <= tats1Sig;
            tats2Reg  <= tats2Sig;
            adderResultReg <= adderResultSig;
            resultReg <= resultSig;
                                                                     
        end if;
    end process;
        
    process(f1Reg, f2Reg, f3Reg, f4Reg, f5Reg, f6Reg, f7Reg, f8Reg, tats1Reg, tats2Reg, adderResultReg)
    begin
        f1SigInp <= (2047 downto 1824 => '0') & f1Reg;
        f2SigInp <= (2047 downto 1824 => '0') & f2Reg;
        f3SigInp <= (2047 downto 1824 => '0') & f3Reg;
        f4SigInp <= (2047 downto 1824 => '0') & f4Reg;
        f5SigInp <= (2047 downto 1824 => '0') & f5Reg;
        f6SigInp <= (2047 downto 1824 => '0') & f6Reg;
        f7SigInp <= (2047 downto 1824 => '0') & f7Reg;
        f8SigInp <= (2047 downto 1824 => '0') & f8Reg;
        
        adder1Inp <= (2943 downto 2048 => tats1Reg(2047)) & tats1Reg(2047 downto 896);
        adder2Inp <= tats2Reg(2047 downto 0);
        
        redInput <= (3584 downto 2944 => adderResultReg(2047)) & adderResultReg(2047 downto 0) & tats1Reg(895 downto 0);
    end process;
    
    result <= (1823 downto 1793 => '0') & resultReg(1792 downto 0);
    
end Behavioral;
