----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/12/2016 10:00:19 AM
-- Design Name: 
-- Module Name: F1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity F7 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
end F7;

architecture Behavioral of F7 is

    signal f1Reg, f2Reg, f3Reg, f4Reg, f5Reg, f6Reg, f7Reg, f8Reg   : STD_LOGIC_VECTOR(1823 downto 0);
    
    signal f1SubSig, f2SubSig, f3SubSig, f4SubSig, 
           f5SubSig, f6SubSig, f7SubSig, f8SubSig                   : STD_LOGIC_VECTOR(2047 downto 0);
    signal sub1Sig, sub1Reg,
           sub2Sig, sub2Reg,
           sub3Sig, sub3Reg,
           sub4Sig, sub4Reg                                         : STD_LOGIC_VECTOR(2048 downto 0);
    signal adder1Sig, adder2Sig, adder3Sig, adder4Sig               : STD_LOGIC_VECTOR(3071 downto 0);
    signal adderResultSig, adderResultReg                           : STD_LOGIC_VECTOR(3072 downto 0);
    
    signal redInput                                                 : STD_LOGIC_VECTOR(3584 downto 0);
    signal resultReg, resultSig                                     : STD_LOGIC_VECTOR(1792 downto 0);
    
    component Reducer is
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            input       : IN STD_LOGIC_VECTOR(3584 downto 0);
            output      : OUT STD_LOGIC_VECTOR(1792 downto 0)
        );
    end component;
       
    component Adder4Inputs3072 is
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(3071 downto 0);
            B           : IN STD_LOGIC_VECTOR(3071 downto 0);
            C           : IN STD_LOGIC_VECTOR(3071 downto 0);
            D           : IN STD_LOGIC_VECTOR(3071 downto 0);
            
            result      : OUT STD_LOGIC_VECTOR(3072  downto 0)
        );
    end component;

    component Subtractor2048 is
        port (
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(2047 downto 0);
            B           : IN STD_LOGIC_VECTOR(2047 downto 0);
            S           : OUT STD_LOGIC_VECTOR(2048 downto 0)
        );
    end component;
    
begin
    
    subF11: Subtractor2048
    port map (
        clk         => clk,
        rst         => rst,
        
        A           => f1SubSig,
        B           => f5SubSig,
        S           => sub1Sig
    );
    
    subF12: Subtractor2048
    port map (
        clk         => clk,
        rst         => rst,
        
        A           => f8SubSig,
        B           => f4SubSig,
        S           => sub2Sig
    );

    subF13: Subtractor2048
    port map (
        clk         => clk,
        rst         => rst,
        
        A           => f7SubSig,
        B           => f3SubSig,
        S           => sub3Sig
    );


    subF14: Subtractor2048
    port map (
        clk         => clk,
        rst         => rst,
        
        A           => f6SubSig,
        B           => f2SubSig,
        S           => sub4Sig
    );
    
    adderF15: Adder4Inputs3072
    port map(
        clk         => clk,
        rst         => rst,
        
        A           => adder1Sig,
        B           => adder2Sig,
        C           => adder3Sig,
        D           => adder4Sig,
        
        result      => adderResultSig
    );
    
    redFinal: Reducer
    port map(
        clk         => clk,
        rst         => rst,
        
        input       => redInput,
        output      => resultSig
    );


    process (clk, rst)
    begin
        if (rst = '1') then
            f1Reg <= (others => '0');
            f2Reg <= (others => '0');
            f3Reg <= (others => '0');
            f4Reg <= (others => '0');
            f5Reg <= (others => '0');
            f6Reg <= (others => '0');
            f7Reg <= (others => '0');
            f8Reg <= (others => '0');
            sub1Reg <= (others => '0');
            sub2Reg <= (others => '0');
            sub3Reg <= (others => '0');
            sub4Reg <= (others => '0');
            adderResultReg  <= (others => '0');
            resultReg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            f1Reg <= f1;
            f2Reg <= f2;
            f3Reg <= f3;
            f4Reg <= f4;
            f5Reg <= f5;
            f6Reg <= f6;
            f7Reg <= f7;
            f8Reg <= f8;
            sub1Reg <= sub1Sig;
            sub2Reg <= sub2Sig;
            sub3Reg <= sub3Sig;
            sub4Reg <= sub4Sig;
            adderResultReg  <= adderResultSig;
            resultReg <= resultSig;
                                                                     
        end if;
    end process;
        
    process(f1Reg, f2Reg, f3Reg, f4Reg, f5Reg, f6Reg, f7Reg, f8Reg, sub1Reg, sub2Reg, sub3Reg, sub4Reg, adderResultReg)
    begin
        f1SubSig <= (2047 downto 1824 => '0') & f1Reg;
        f2SubSig <= (2047 downto 1824 => '0') & f2Reg;
        f3SubSig <= (2047 downto 1824 => '0') & f3Reg;
        f4SubSig <= (2047 downto 1824 => '0') & f4Reg;
        f5SubSig <= (2047 downto 1824 => '0') & f5Reg;
        f6SubSig <= (2047 downto 1824 => '0') & f6Reg;
        f7SubSig <= (2047 downto 1824 => '0') & f7Reg;
        f8SubSig <= (2047 downto 1824 => '0') & f8Reg;
        
        adder1Sig <= (3519 downto 2048 => sub1Reg(2047)) & sub1Reg(2047 downto 448);
        adder2Sig <= (3519 downto 2496 => sub2Reg(2047)) & sub2Reg(2047 downto 0);
        adder3Sig <= (3519 downto 2944 => sub3Reg(2047)) & sub3Reg(2047 downto 0) & (895 downto 448 => '0');
        adder4Sig <= (3519 downto 3392 => sub4Reg(2047)) & sub4Reg(2047 downto 0) & (1343 downto 448 => '0');
        
        redInput <= (3584 downto 3393 => adderResultReg(2945)) & adderResultReg(2944 downto 0) & sub1Reg(447 downto 0);
        
    end process;
    
    result <= (1823 downto 1793 => '0') & resultReg(1792 downto 0);
    
end Behavioral;
