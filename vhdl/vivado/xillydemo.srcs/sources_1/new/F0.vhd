----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 06:14:58 PM
-- Design Name: 
-- Module Name: F0 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2016 03:44:47 PM
-- Design Name: 
-- Module Name: Adder8Inputs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity F0 is
    port(
        clk         : IN STD_LOGIC;
        rst         : IN STD_LOGIC;
        
        f1     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f2     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f3     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f4     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f5     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f6     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f7     : IN STD_LOGIC_VECTOR(1823 downto 0);
        f8     : IN STD_LOGIC_VECTOR(1823 downto 0);
        
        result      : OUT STD_LOGIC_VECTOR(1823  downto 0)
    );
end F0;

architecture Behavioral of F0 is

    signal f1Reg, f2Reg, f3Reg, f4Reg, f5Reg, f6Reg, f7Reg, f8Reg   : STD_LOGIC_VECTOR(1823 downto 0);
    signal adderReg, adderSig                                       : STD_LOGIC_VECTOR(2048 downto 0);
    
    signal redInput                                                 : STD_LOGIC_VECTOR(3584 downto 0);
    signal resultReg, resultSig                                     : STD_LOGIC_VECTOR(1792 downto 0);
    
    component Reducer is
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            input       : IN STD_LOGIC_VECTOR(3584 downto 0);
            output      : OUT STD_LOGIC_VECTOR(1792 downto 0)
        );
    end component;
       
    component Adder8Inputs is
        generic(INPUT_LENGTH: INTEGER);
        port(
            clk         : IN STD_LOGIC;
            rst         : IN STD_LOGIC;
            
            A           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            B           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            C           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            D           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            E           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            F           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            G           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            H           : IN STD_LOGIC_VECTOR(INPUT_LENGTH-1 downto 0);
            
            result      : OUT STD_LOGIC_VECTOR(2048  downto 0)
        );
    end component;

begin


    adderF0: Adder8Inputs
    generic map (
        INPUT_LENGTH => 1824
    )
    port map(
        clk         => clk,
        rst         => rst,
        
        A     => f1Reg,
        B     => f2Reg,
        C     => f3Reg,
        D     => f4Reg,
        E     => f5Reg,
        F     => f6Reg,
        G     => f7Reg,
        H     => f8Reg,
        
        result      => adderSig
    );
    
    
    reducerF0: Reducer
    port map(
       clk         => clk,
       rst         => rst,
       
       input       => redInput,  
       output      => resultSig
    );

    process (clk, rst)
    begin
        if (rst = '1') then
            f1Reg <= (others => '0');
            f2Reg <= (others => '0');
            f3Reg <= (others => '0');
            f4Reg <= (others => '0');
            f5Reg <= (others => '0');
            f6Reg <= (others => '0');
            f7Reg <= (others => '0');
            f8Reg <= (others => '0');
            adderReg  <= (others => '0');
            resultReg <= (others => '0');
            
        elsif (rising_edge(clk)) then
            f1Reg <= f1;
            f2Reg <= f2;
            f3Reg <= f3;
            f4Reg <= f4;
            f5Reg <= f5;
            f6Reg <= f6;
            f7Reg <= f7;
            f8Reg <= f8;
            adderReg  <= adderSig;
            resultReg <= resultSig;
                                                                     
        end if;
    end process;
        
    process(adderReg)
    begin
        redInput <= (3584 downto 2049 => '0') & adderReg;
    end process;
    
    result <= (1823 downto 1793 => '0') & resultReg(1792 downto 0);
    
end Behavioral;
